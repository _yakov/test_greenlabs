
$(function(){
	//part 1
	$('.read-more').on('click', function(e){
		$.getJSON('news.json', function(data){
			var items = [],
				html;
			$.each(data, function(key, val){
				//if image undefined
				if(!val.image){
					val.image = 'img/tmp/default.jpg';
				}
				if((key % 2) == 0){
					items.push('<article class="news__item">'+
						'<div class="col-4 news__img"><img src="'+val.image+'" alt=""></div>'+
						'<div class="col-6 news__body">'+
							'<div class="news__date">'+val.date+'</div>'+
							'<h2 class="news__title"><a href="">'+val.title+'</a></h2>'+
							'<p class="news__text">'+val.preview+'</p>'+
						'</div>'+
						'</article>');
				}else{
					items.push('<article class="news__item">'+
						'<div class="col-6 news__body">'+
							'<div class="news__date">'+val.date+'</div>'+
							'<h2 class="news__title"><a href="">'+val.title+'</a></h2>'+
							'<p class="news__text">'+val.preview+'</p>'+
						'</div>'+
						'<div class="col-4 news__img"><img src="'+val.image+'" alt=""></div>'+
						'</article>');
				}
			});
			html = items.join('');
			$(html).appendTo($('.news'));
		});
	});
	//part-2
	$("#form").validate({
		rules : {
			user : {required : true},
			phone:{
				 require_from_group: [1, ".phone-group"]
				
			},
			email: {
				 require_from_group: [1, ".phone-group"]

			},
			usr_text: {
				required:true
			}
		},
		messages : {
			user : {
				required : "Введите ваше имя",
			},
			phone:{
				require_from_group: "Введите телефон",
				
			},
			email: {
				require_from_group :"Введите правильный email",

			},
			usr_text: {
				required : "Введите текст",
			}
		},
		highlight: function(element, errorClass) {
		     $(element).addClass(errorClass);
		  },
		unhighlight: function(element, errorClass) {
		     $(element).removeClass(errorClass);
		     $(element).addClass('valid');
		},
		submitHandler: function (form) {
		          $.ajax({
		              dataType: "json",
		              url: "sucsess.json",
		              data: $(form).serialize(),
		              success: function (data) {
		              	console.log(data)
		              		if(data.success){
		              			$("<div id='message'></div>").appendTo($('.form'));
		              			$('#message').html("<h2>"+data.msg+"</h2>").hide().fadeIn(1500);
		              			$(form).fadeOut()
		              		}else{
		              			$("<div id='message'></div>").appendTo($('.form'));
		              			$('#message').html("<h2>"+data.msg+"</h2>").hide().fadeIn(1500);
		              			$(form).fadeOut()
		              		}
		                 
		              }
		          });
		          return false; // required to block normal submit since you used ajax
		      }
	});
	$('.modal--show').on('click', function(e){
		e.stopPropagation()
		$('.overlay').fadeIn();
	})
	$('.modal__close').on('click', function(e){
		e.stopPropagation();
		$('.overlay').fadeOut(function(){
			$('#form input').val('');
			$('#form').fadeIn();
		});

	})
	$(document).mouseup(function (e)
	{
	    var container = $(".form");
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        $('.overlay').fadeOut();
	    }
	});
	//part-4
	var $root = $('html, body');
	$('.nav__item').click(function() {
	    var href = $.attr(this, 'href');
	    $root.stop().animate({
	        scrollTop: $(href).offset().top
	    }, 1000, function () { 
	        window.location.hash = href;
	    });
	    return false;
	});
	//part - 5
	var navMobile = function(){
		$('#nav-icon1').on('click', function(){
			if($('.nav').is('.open-nav')){
				$('.nav').stop().animate({
					right: -$('.nav').outerWidth()
				},function(){
					$(this).removeClass('open-nav')
					$('#nav-icon1').removeClass('open')
				})
			}else{
				$('.nav').stop().animate({
					right: 0
				},function(){
					$(this).addClass('open-nav')
					$('#nav-icon1').addClass('open')
				})
			}
			
		})
	}
	if($(window).width() <= 768){
		navMobile()
	}
	$(window).resize(function(){
		if($(window).width() <= 768){
			navMobile()
			$('.nav').css({
				right: -$('.nav').outerWidth()
			})
		}else{
			$('.nav').css({
				right: 'auto'
			})
			$('.nav').removeClass('open-nav')
		}
	});
});